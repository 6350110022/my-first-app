import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirsApp());
}

class MyFirsApp extends StatelessWidget {
  const MyFirsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon),
          title: Text('My First App'),
          backgroundColor: Colors.pink,
          actions: [
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.add_alarm),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.access_time),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search),
            ),
          ],

        ),
        backgroundColor: Colors.pinkAccent,
        body: Column (
          children:[
            CircleAvatar(
              backgroundColor: Colors.black,
              radius: 250,
              child: CircleAvatar(
                  radius: 190,
                  backgroundImage: AssetImage('assets/onin.png')
              ),
            ),
            Text('นางสาวอรอินทุ์ หวันสู',style:TextStyle(height: 2,
              fontSize: 30, fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,)),
            Text('6350110022',style:TextStyle(height:1, fontSize: 20)),
          ],

        ),
      ),
    );
  }
}